Repdrive Project
=========

This application is a demo for Repdrive created by Aric Beagley.

Version
----

0.0.1

* * *

Tech
-----------

###Client:

The primary tech behind this application is Angular utilizing Yeoman and the official Angular generator:

* [Yeoman](http://yeoman.io/) - Sweet generator for scaffolding web applications
* [generator-angular](https://github.com/yeoman/generator-angular) - The official Angular scaffold for Yeoman
* [Twitter Bootstrap](http://getbootstrap.com/) - great UI boilerplate for modern web apps

###Server:

The server is a basic pass-through service for hitting the test api.

###Ionic App:

TBD

* * *

Installation
--------------

###Client:

```sh
npm install -g grunt-cli bower yo generator-angular
git clone git@bitbucket.org:abeagley/repdrive-project.git
cd repdrive-project/client && npm install && bower install
grunt serve
```

###Server:

TBD

###Ionic App:

TBD
