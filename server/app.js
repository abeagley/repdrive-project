var express = require('express'),
    app = express(),
    Client = require('node-rest-client').Client,
    rClient = new Client();
    port = 3000,
    apiUrl = 'https://secure.reviewtrackers.com/api',
    apiKey = '274b859d23b5e2778b46';

app.get('/locations/:id', function(req, res) {
  rClient.get(apiUrl + '/locations/' + req.params.id + '?api_token=' + apiKey, function(data, resp) {
    res.json(JSON.parse(data));
  });
});

app.get('/locations?*', function(req, res){
  console.log("Yup we're going.");
  res.sendStatus(200);
});

app.listen(port);
console.log("Server listening on port " + port);