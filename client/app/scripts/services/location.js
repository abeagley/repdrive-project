'use strict';

/**
 * @ngdoc service
 * @name repdriveApp.services/location
 * @description
 * # location
 * Service in the repdriveApp.
 */
angular.module('repdriveApp')
  .service('LocationSvc', function($http, APP_CONSTANTS) {
    return {
      find: function(params) {
        return $http({
          method: 'GET',
          url: APP_CONSTANTS.apiUrl + '/locations',
          params: params
        });
      },
      findById: function(id) {
        return $http({
          method: 'GET',
          url: APP_CONSTANTS.apiUrl + '/locations/' + id
        });
      }
    };
  });
