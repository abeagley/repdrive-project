'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */
angular
  .module('repdriveApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngSanitize',
    'ngTouch',
    'ui.router'
  ])
  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('app', {
        url: '/app',
        controller: 'AppCtrl',
        templateUrl: 'views/app.html'
      })
      .state('app.showLocation', {
        url: '/locations/:id',
        controller: 'LocationShowCtrl',
        templateUrl: 'views/locations/show.html'
      });

    // Hardcoded to Ken Garff Volkswagen for now
    $urlRouterProvider.otherwise('/app/locations/18598');

  });
