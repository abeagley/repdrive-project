'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:AppCtrl
 * @description
 * # AppCtrl
 * Controller of the repdriveApp
 */
angular.module('repdriveApp')
  .controller('AppCtrl', function ($scope) {

    $scope.isLoading = false;

  });
