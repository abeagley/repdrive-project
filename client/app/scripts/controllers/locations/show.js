'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:LocationShowCtrl
 * @description
 * # LocationShowCtrl
 * Controller of the repdriveApp
 */
angular.module('repdriveApp')
  .controller('LocationShowCtrl', function ($rootScope, $scope, $stateParams, LocationSvc) {

    $rootScope.isLoading = true;

    // Fetch our Location based on the id param in the URL
    LocationSvc.findById($stateParams.id)
      .then(function(resp) {
        console.log(resp.data);
        // On success
        console.log("success");

        $rootScope.isLoading = false;
      }, function(err) {
        // On error
        console.log("Something went wrong.");
      });

    console.log($stateParams);
    console.log("Herro there");

  });
