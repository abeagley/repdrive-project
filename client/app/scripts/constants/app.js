'use strict';

/**
 * @ngdoc constant
 * @name repdriveApp.constants/app
 * @description
 * # constants/app
 * Constant in the repdriveApp.
 */
angular.module('repdriveApp')
  .constant('APP_CONSTANTS', {
    apiUrl: 'http://localhost:3000'
  });
